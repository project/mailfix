<?php
/**
 * @file
 * Included by mailfix.module to perform Mailfix domain management operations.
 */
 
/**
 * Form submit callback; confirms deleting a domain.
 */
function mailfix_domain_delete_submit($form, &$form_state) {
  $result = db_fetch_object(db_query("SELECT count(uid) AS users FROM {mailfix_users} WHERE domain_id = %d", $form_state['values']['domain_id']));
  db_query(
    'DELETE FROM {mailfix_users} WHERE domain_id = %d', 
    $form_state['values']['domain_id']);
  db_query(
    'DELETE FROM {mailfix_domains} WHERE domain_id = %d', 
    $form_state['values']['domain_id']);

  drupal_set_message(t('Domain %domain_name has been deleted. %domain_users.', 
    array(
      '%domain_name' => $form_state['values']['domain_name'],
      '%domain_users' => format_plural($result->users, '1 Mailfix user profile was deleted', '@count Mailfix user profiles were deleted'),
    )));
  watchdog(
    'mailfix', 
    'Domain %domain_name has been deleted. %domain_users.', 
    array(
      '%domain_name' => $form_state['values']['domain_name'],
      '%domain_users' => format_plural($result->users, '1 Mailfix user profile was deleted', '@count Mailfix user profiles were deleted'),
    ), 
    WATCHDOG_NOTICE, 
    l('view', 'admin/settings/mailfix')
  );

  $form_state['redirect'] = 'admin/settings/mailfix';
  return;
}  // mailfix_domain_delete_confirm

/**
 * Menu callback. Deletes specified domain.
 */
function mailfix_domain_delete(&$form_state, $domain_id) {
  $domain = db_fetch_object(db_query('SELECT domain_name FROM {mailfix_domains} d WHERE d.domain_id = %d', $domain_id));
  if (!$domain) {
    drupal_not_found();
    return;
  }
  $form['domain_id'] = array('#type' => 'value', '#value' => $domain_id);
  $form['domain_name'] = array('#type' => 'value', '#value' => $domain->domain_name);
  return confirm_form(
    $form,
    t('Are you sure you want to delete the domain <em>%domain</em>?', array('%domain' => $domain->domain_name)), 
    'admin/settings/mailfix/list',  // if user denies the action this is the landing path
    t('This action cannot be undone. All exising email addresses belonging to this domain will no longer be managed by this mail server.'),
    t('Delete'), 
    t('Cancel')
  );
}

/**
 * Menu callback: Generate a form to add an existing domain
 *
 * @see mailfix_add_domain_form_validate()
 * @see mailfix_add_domain_form_submit()
 */
function mailfix_add_domain_form(&$form_state, $arg = NULL) {
  $form['#redirect'] = 'admin/settings/mailfix';
  $form['domain_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#description' => t('Enter a valid domain name for which this Drupal server is intended to manage mail, e.g. example.com.'),
    '#required' => TRUE,
    '#weight' => 1,
  );
  $form['default_quota'] = array(
    '#type' => 'textfield',
    '#default_value' => 104857600, // default value for quota 100MB = 1024 * 1024 * 100 = 104857600 bytes
    '#title' => t('Default storage quota'),
    '#description' => t('Enter the default mailbox storage quota (in bytes). Default value of 104857600 corresponds to 100MB. For unlimited quota set this value to 0.'),
    '#required' => TRUE,
    '#weight' => 2,
  );
  $form['submit'] = array('#type' => 'submit',
    '#value' => t('Add domain'),
    '#weight' => 1000,
  );
  return $form;
}  // mailfix_add_domain_form

/**
 * Implementation of hook_validate().
 * 
 * Validate mailfix_add_domain_form form submissions for valid domain names.
 */
function mailfix_add_domain_form_validate($form, &$form_state) {
  if (!preg_match('/^([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$/i', $form_state['values']['domain_name'])) {
    form_set_error('domain name', t('The specified domain name contains one or more illegal characters. Spaces or any other special characters except dash (-) and underscore (_) are not allowed.'));
  }
  if ($form_state['values']['quota'] < 0) {
    form_set_error('mail storage quota', t('The mail storage quota must be a positive integer value.'));
  }
  if (db_result(db_query("SELECT d.domain_id FROM {mailfix_domains} d WHERE domain_name = '%s'", 
      $form_state['values']['domain_name']))) {
    form_set_error('title', t('The specified domain name is already registered.'));
  }
}  // mailfix_add_domain_form_validate

/**
 * Process mailfix_add_domain_form form submissions to add new domain.
 */
function mailfix_add_domain_form_submit($form, &$form_state) {
  db_query(
    "INSERT INTO {mailfix_domains} (domain_name, default_quota) VALUES ('%s', %d)",
    $form_state['values']['domain_name'],
    $form_state['values']['default_quota']
  );
  $sql = "SELECT * FROM {mailfix_domains} WHERE domain_name = '%s' ";
  $domain = db_fetch_object(db_query($sql, $form_state['values']['domain_name']));

  if ($domain->domain_id > 0) {
    // Clean out any wrongly associated uid, as this field is a unique index
    $sql = "DELETE FROM {mailfix_users} WHERE uid IN (SELECT uid FROM {users} u WHERE u.mail LIKE '%@%s')";
    db_query($sql, $domain->domain_name);
    
    // Populate mailfix_users
    $sql = "INSERT INTO {mailfix_users} (uid, domain_id) SELECT uid, %d AS domain_id FROM {users} WHERE mail LIKE '%@%s'";
    db_query($sql, $domain->domain_id, $domain->domain_name);
    }
  // How many Mailfix profiles were created?
  $sql = "SELECT count(uid) AS users FROM {mailfix_users} WHERE domain_id = %d";
  $result = db_fetch_object(db_query($sql, $domain->domain_id));
  drupal_set_message(t('Domain name %domain_name created. %domain_users with default settings.', array(
      '%domain_name' => $form_state['values']['domain_name'], 
      '%domain_users' => format_plural($result->users, '1 user was found and configured', '@count users were found and configured'),
    )
  ));
  watchdog(
    'mailfix', 
    'Domain name %domain_name created. %domain_users with default settings.', 
    array(
      '%domain_name' => $form_state['values']['domain_name'], 
      '%domain_users' => format_plural($result->users, '1 user was found and configured', '@count users were found and configured'),
    ),
    WATCHDOG_NOTICE, 
    l('view', 'admin/settings/mailfix')
  );
} // mailfix_add_domain_form_submit
 
/**
 * Menu callback: Generate a form to edit an existing domain
 *
 * @see mailfix_edit_domain_form_validate()
 * @see mailfix_edit_domain_form_submit()
 */
function mailfix_edit_domain_form(&$form_state, $domain_id) {
  $domain = db_fetch_object(db_query('SELECT domain_id, domain_name, default_quota FROM {mailfix_domains} d WHERE d.domain_id = %d', $domain_id));
  if (!$domain) {
    drupal_not_found();
    return;
  }
  $form['domain_id'] = array('#type' => 'value', '#value' => $domain_id);
  $form['domain_name'] = array('#type' => 'value', '#value' => $domain->domain_name);
  $form['#redirect'] = 'admin/settings/mailfix';
  $form['domain_label'] = array(
    '#type' => 'item',
    '#title' => t('Domain name'),
    '#description' => t('This is the registered domain name for which this Drupal server is intended to manage mail.'),
    '#value' => $domain->domain_name,
  );
  $form['default_quota'] = array(
    '#type' => 'textfield',
    '#default_value' => $domain->default_quota, 
    '#title' => t('Default storage quota'),
    '#description' => t('This is the default mailbox storage quota (in bytes). Default value of 104857600 corresponds to 100MB. For unlimited quota set this value to 0.'),
    '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit',
    '#value' => t('Update domain'),
    '#weight' => 1000,
  );
  return $form;
}  // mailfix_edit_domain_form

/**
 * Implementation of hook_validate().
 * 
 * Validate mailfix_edit_domain_form form submissions for valid quota.
 */
function mailfix_edit_domain_form_validate($form, &$form_state) {
  if ($form_state['values']['quota'] < 0) {
    form_set_error('mail storage quota', t('The mail storage quota must be a positive integer value.'));
  }
}  // mailfix_edit_domain_form_validate

/**
 * Process mailfix_edit_domain_form form submissions to update existing domain.
 */
function mailfix_edit_domain_form_submit($form, &$form_state) {
  db_query(
    "UPDATE {mailfix_domains} SET default_quota = %d WHERE domain_id = %d",
    $form_state['values']['default_quota'],
    $form_state['values']['domain_id']
  );
  $sql = "SELECT * FROM {mailfix_domains} WHERE domain_id = %d ";
  $domain = db_fetch_object(db_query($sql, $form_state['values']['domain_name']));
  // Alert user of update operation.
  drupal_set_message(t('Default quota for domain %domain_name updated.', array(
      '%domain_name' => $form_state['values']['domain_name'], 
    )
  ));
} // mailfix_edit_domain_form_submit

/**
 * Menu callback. Displays domains configuration form.
 */
function mailfix_domains_overview() {
  // Render domains list.
  $sql = 'SELECT d.domain_id, d.domain_name, d.default_quota, count(u.uid) AS domain_users '
    .'FROM {mailfix_domains} d LEFT JOIN {mailfix_users} u ON d.domain_id = u.domain_id '
    .'GROUP BY  d.domain_id, d.domain_name, d.default_quota '
    .'ORDER BY d.domain_name';
  $result = db_query($sql);
  $header = array(
    t('Domains'), 
    t('Current users'), 
    t('Default quota'), 
//    t('Operations'),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $rows = array();

  while ($domain = db_fetch_object($result)) {
    $row = array(
      $domain->domain_name, 
      $domain->domain_users, 
      format_size($domain->default_quota), 
//      l(t('delete'), 'admin/settings/mailfix/delete/'. $domain->domain_id),
    );
    // Set the edit column
    $row[] = array('data' => l(t('edit'), 'admin/settings/mailfix/edit/'. $domain->domain_id), ) ;
    // Set the delete column
    $row[] = array('data' => l(t('delete'), 'admin/settings/mailfix/delete/'. $domain->domain_id), ) ;
    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No registered mail domains were found.'), 'colspan' => 5, 'class' => 'message'));
  }

  return theme('table', $header, $rows);
}  // mailfix_domains_overview

