<?php
/**
 * @file
 * Generic functions shared by Mailfix functionality.
 */

define('MAILFIX_ADMIN_ACCESS_NONE', 0);
define('MAILFIX_ADMIN_ACCESS_HIDE_ADMIN_FIELDS', 1);
define('MAILFIX_ADMIN_ACCESS_READONLY_ADMIN_FIELDS', 2);
define('MAILFIX_ADMIN_ACCESS_FULL', 3);

/**
 * Helper function verifying if an email address belongs to registered domains.
 *
 * When user profile is being updated, we must verify if provided mail address
 * belongs to registered domains.
 *
 * @param $email
 *    Email address who's domain will be checked.
 * @return 
 *    0 if email's domain is not a registered one
 *    positive integer otherwise, corresponding to domain_id
 */
function mailfix_domain_id_from_email($email) {
  $domain = drupal_substr(strrchr($email, "@"), 1); // extract domain
  $domain_id = 0;
  $sql = "SELECT domain_id FROM {mailfix_domains} WHERE domain_name = '%s'";
  if ($result = db_fetch_object(db_query($sql, $domain))) {
    $domain_id = $result->domain_id;
  }
  return $domain_id;
} // mailfix_domain_id_from_email

function mailfix_perm_debug($message = NULL, $type = 'status', $repeat = TRUE)
{
  if (0)
    drupal_set_message($message, $type, $repeat);
}


function __mailfix_get_access_level($type, $account, &$old_dom_id = NULL)
{
  global $user;

  if (user_access('administer users')) {
    mailfix_perm_debug(t('Current user has "Administer users" permission.'));
    return MAILFIX_ADMIN_ACCESS_FULL;
  }

  if ($type == 'delete') {
    $compulsory = module_invoke_all('mailfix_compulsory', $account);
    if ($compulsory && $compulsory[0] == 1) {
      mailfix_perm_debug(t('Compulsory account.'));
      return MAILFIX_ADMIN_ACCESS_NONE;
    }
  }

  if ($user->uid == 1 || user_access('administer users')) {
    mailfix_perm_debug(t('User 1 or administer users: Write access'));
    return MAILFIX_ADMIN_ACCESS_FULL;
  }

  if ($user->uid == $account->uid) {
    mailfix_perm_debug(t('Editing own account.'));
    return MAILFIX_ADMIN_ACCESS_READONLY_ADMIN_FIELDS;
  }

  if ($account->uid == 1) {
    mailfix_perm_debug(t('Not giving access to edit user 1.'));
    return MAILFIX_ADMIN_ACCESS_NONE;
  }

  $access = module_invoke_all('mailfix_access', $type, $account);
  if ($access) {
    mailfix_perm_debug(t('Invoking mailfix access returned %access.'), array('%access' => $access[0]));
    return $access[0];
  }

  mailfix_perm_debug(t('Fell through to returning no access.'));
  return MAILFIX_ADMIN_ACCESS_NONE;
}

function mailfix_get_access_level($type, $account, &$old_dom_id = NULL)
{
  static $cache = array();

  if (!isset($cache[$account->uid][$type]))
    $cache[$account->uid][$type] = __mailfix_get_access_level($type, $account, $old_dom_id);
  return $cache[$account->uid][$type];
}

